# APS2020

Slides and video recording for my talk "Mean-velocity scaling of compressible turbulent boundary layer flows under non-adiabatic wall conditions" at APS DFD 2020, originally scheduled to take place in Chicago. 

The slides are available in pdf format, while the video is provided in mp4. The full meeting link from APS can accessed using: http://meetings.aps.org/Meeting/DFD20/Session/E09.11

For any questions and/or comments, please email me at: kyounes@uwaterloo.ca